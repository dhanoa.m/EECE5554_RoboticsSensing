% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 812.739935578281120 ; 820.317820255996594 ];

%-- Principal point:
cc = [ 395.448627014057479 ; 522.855509316545749 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.031432148641703 ; -0.090395772710574 ; -0.000526250802454 ; 0.001292696968427 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 9.091972269081545 ; 9.310035093632729 ];

%-- Principal point uncertainty:
cc_error = [ 4.030251889859005 ; 4.747948036473198 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.014884077839655 ; 0.033553628860361 ; 0.001978648226673 ; 0.001741397650266 ; 0.000000000000000 ];

%-- Image size:
nx = 780;
ny = 1040;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 7;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 2.788011e+00 ; 3.478306e-01 ; -1.200996e-01 ];
Tc_1  = [ -1.489182e+02 ; 5.651875e+01 ; 3.132078e+02 ];
omc_error_1 = [ 6.729622e-03 ; 2.487266e-03 ; 8.456402e-03 ];
Tc_error_1  = [ 1.571405e+00 ; 1.880648e+00 ; 3.583950e+00 ];

%-- Image #2:
omc_2 = [ 2.788011e+00 ; 3.478306e-01 ; -1.200996e-01 ];
Tc_2  = [ -1.489182e+02 ; 5.651875e+01 ; 3.132078e+02 ];
omc_error_2 = [ 6.729622e-03 ; 2.487266e-03 ; 8.456402e-03 ];
Tc_error_2  = [ 1.571405e+00 ; 1.880648e+00 ; 3.583950e+00 ];

%-- Image #3:
omc_3 = [ -2.790389e+00 ; -1.206689e-01 ; -2.095526e-02 ];
Tc_3  = [ -1.404885e+02 ; 1.558638e+02 ; 3.586714e+02 ];
omc_error_3 = [ 6.593765e-03 ; 2.064590e-03 ; 8.187672e-03 ];
Tc_error_3  = [ 1.808371e+00 ; 2.081575e+00 ; 3.739614e+00 ];

%-- Image #4:
omc_4 = [ -3.025676e+00 ; -2.669520e-02 ; 3.825696e-01 ];
Tc_4  = [ -1.279868e+02 ; 1.579354e+02 ; 3.285779e+02 ];
omc_error_4 = [ 6.462829e-03 ; 1.973658e-03 ; 8.757659e-03 ];
Tc_error_4  = [ 1.653256e+00 ; 1.910248e+00 ; 3.569774e+00 ];

%-- Image #5:
omc_5 = [ 3.060012e+00 ; 2.427483e-01 ; -2.607521e-01 ];
Tc_5  = [ -1.399552e+02 ; 6.927518e+01 ; 3.042378e+02 ];
omc_error_5 = [ 6.667424e-03 ; 2.152751e-03 ; 9.264237e-03 ];
Tc_error_5  = [ 1.495882e+00 ; 1.800383e+00 ; 3.402542e+00 ];

%-- Image #6:
omc_6 = [ 2.863198e+00 ; 3.257354e-01 ; 2.144305e-01 ];
Tc_6  = [ -1.133769e+02 ; 8.685779e+01 ; 2.529856e+02 ];
omc_error_6 = [ 6.418936e-03 ; 1.782733e-03 ; 8.340657e-03 ];
Tc_error_6  = [ 1.334003e+00 ; 1.540265e+00 ; 2.995012e+00 ];

%-- Image #7:
omc_7 = [ 2.899595e+00 ; -2.253508e-01 ; 4.941425e-01 ];
Tc_7  = [ -6.291539e+01 ; 1.320153e+02 ; 2.516629e+02 ];
omc_error_7 = [ 6.329727e-03 ; 2.422877e-03 ; 8.490655e-03 ];
Tc_error_7  = [ 1.329924e+00 ; 1.541782e+00 ; 3.054702e+00 ];

