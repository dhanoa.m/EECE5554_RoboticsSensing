% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 816.801153405223658 ; 824.611512692416682 ];

%-- Principal point:
cc = [ 395.169679182898051 ; 523.633715665879208 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.033952713177011 ; -0.097629749024205 ; -0.000569825163652 ; 0.001539078230517 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 8.315545155227108 ; 8.440887294858994 ];

%-- Principal point uncertainty:
cc_error = [ 3.813239290328843 ; 4.514016777185368 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.014146316672203 ; 0.032772257091163 ; 0.001843779451180 ; 0.001621153003729 ; 0.000000000000000 ];

%-- Image size:
nx = 780;
ny = 1040;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 8;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ 2.786915e+00 ; 3.477782e-01 ; -1.200641e-01 ];
Tc_1  = [ -1.488173e+02 ; 5.618905e+01 ; 3.147613e+02 ];
omc_error_1 = [ 6.356128e-03 ; 2.411823e-03 ; 8.132056e-03 ];
Tc_error_1  = [ 1.487049e+00 ; 1.788763e+00 ; 3.278461e+00 ];

%-- Image #2:
omc_2 = [ 2.786915e+00 ; 3.477782e-01 ; -1.200641e-01 ];
Tc_2  = [ -1.488173e+02 ; 5.618905e+01 ; 3.147613e+02 ];
omc_error_2 = [ 6.356128e-03 ; 2.411823e-03 ; 8.132056e-03 ];
Tc_error_2  = [ 1.487049e+00 ; 1.788763e+00 ; 3.278461e+00 ];

%-- Image #3:
omc_3 = [ -2.788583e+00 ; -1.204940e-01 ; -2.133872e-02 ];
Tc_3  = [ -1.403891e+02 ; 1.554888e+02 ; 3.604658e+02 ];
omc_error_3 = [ 6.180853e-03 ; 1.986813e-03 ; 7.859662e-03 ];
Tc_error_3  = [ 1.709307e+00 ; 1.973634e+00 ; 3.367737e+00 ];

%-- Image #4:
omc_4 = [ -3.025013e+00 ; -2.689093e-02 ; 3.832107e-01 ];
Tc_4  = [ -1.278857e+02 ; 1.576346e+02 ; 3.302313e+02 ];
omc_error_4 = [ 6.117397e-03 ; 1.920685e-03 ; 8.358278e-03 ];
Tc_error_4  = [ 1.557887e+00 ; 1.813254e+00 ; 3.230638e+00 ];

%-- Image #5:
omc_5 = [ 3.059804e+00 ; 2.427547e-01 ; -2.608703e-01 ];
Tc_5  = [ -1.398648e+02 ; 6.900881e+01 ; 3.057557e+02 ];
omc_error_5 = [ 6.376486e-03 ; 2.080573e-03 ; 8.877003e-03 ];
Tc_error_5  = [ 1.413262e+00 ; 1.710046e+00 ; 3.104624e+00 ];

%-- Image #6:
omc_6 = [ 2.862181e+00 ; 3.256207e-01 ; 2.153761e-01 ];
Tc_6  = [ -1.132623e+02 ; 8.654953e+01 ; 2.542917e+02 ];
omc_error_6 = [ 6.067277e-03 ; 1.747142e-03 ; 8.058988e-03 ];
Tc_error_6  = [ 1.261260e+00 ; 1.464056e+00 ; 2.738705e+00 ];

%-- Image #7:
omc_7 = [ 2.898940e+00 ; -2.253769e-01 ; 4.952351e-01 ];
Tc_7  = [ -6.279077e+01 ; 1.317186e+02 ; 2.530697e+02 ];
omc_error_7 = [ 6.025862e-03 ; 2.342560e-03 ; 8.206158e-03 ];
Tc_error_7  = [ 1.256856e+00 ; 1.466057e+00 ; 2.787040e+00 ];

%-- Image #8:
omc_8 = [ 2.898940e+00 ; -2.253769e-01 ; 4.952351e-01 ];
Tc_8  = [ -6.279077e+01 ; 1.317186e+02 ; 2.530697e+02 ];
omc_error_8 = [ 6.025862e-03 ; 2.342560e-03 ; 8.206158e-03 ];
Tc_error_8  = [ 1.256856e+00 ; 1.466057e+00 ; 2.787040e+00 ];

