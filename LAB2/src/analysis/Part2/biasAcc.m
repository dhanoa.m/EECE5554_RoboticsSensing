clear all

imuInCar = load ('imuInCar.mat')
imu_data = imuInCar;
imu_time = imu_data.C(:,1);
imu_time = table2array(imu_time);
imu_orientation = imu_data.C(:,23:26);
imu_ang_vel = imu_data.C(:,36:38);
imu_lin_acc = imu_data.C(:,48:50);
imu_lin_acc = table2array(imu_lin_acc);
mag_measurements = imu_data.C(:,8:10);
mag_measurements = table2array(mag_measurements);

gpsInCar = load('gpsInCar.mat')
gps_data = gpsInCar;
gps_time = gps_data.A(:,1);
gps_time = table2array(gps_time);
gps_measurements = gps_data.A(:,8:9);
gps_measurements = table2array(gps_measurements);


%Bias subtraction from acceleration
for i = 1:39745
    bias = [mean(imu_lin_acc(i,1)) mean(imu_lin_acc(i,2)) mean(imu_lin_acc(i,3))];
    imu_lin_acc_unbiased = [imu_lin_acc(:,1)-bias(1) imu_lin_acc(:,2)-bias(2) imu_lin_acc(:,3)-bias(3)];
end
gps_time_lag = imu_time(1)-gps_time(1);
gps_time_matched = gps_time+gps_time_lag;


imu_time_real = imu_time(34856:35877,:);
imu_orientation_real = imu_orientation(34856:35877,:);
imu_ang_vel_real = imu_ang_vel(34856:35877,:);
imu_lin_acc_real = imu_lin_acc_unbiased(34856:35877,:);

mag_time_real = imu_time(34856:35877,:);
mag_measurements_real = mag_measurements(34856:35877,:);
%{
gps_time_real = gps_time_matched(270:1300,:);
gps_measurements_real = gps_measurements(270:1300,:);
%}

