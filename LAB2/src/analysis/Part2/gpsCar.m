clear all

gpsInCar = load('gpsInCar.mat')
gps_data = gpsInCar;

gps_time = gps_data.A(:,1);
gps_measurements = gps_data.A(:,8:9);
gps_measurements = table2array(gps_measurements);

%GPS Plot
figure
plot(gps_measurements(:,1), gps_measurements(:,2));
title('GPS Plot');
title('Plot of GPS trajectory'); 
xlabel('UTM Easting'); 
ylabel('UTM Northing');