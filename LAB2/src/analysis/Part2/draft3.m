clear all

gpsInCar = load ('gpsInCar.mat')

imuInCar = load ('imuInCar.mat')

timeg = gpsInCar.A(:,1);
utm_east = gpsInCar.A(:,8);
utm_north = gpsInCar.A(:,9);
east = table2array(utm_east);
north = table2array(utm_north);
time_gps = table2array(timeg);
velge = diff(east);
timedg = diff(time_gps);
vel_x = velge/timedg;
velgn = diff(north);
vel_y = velgn/timedg;
lat = gpsInCar.A(:,5);
long = gpsInCar.A(:,6);

timeic = imuInCar.C(:,1);
ox = imuInCar.C(:,23);
oy = imuInCar.C(:,24);
oz = imuInCar.C(:,25);
ow = imuInCar.C(:,26);
c_mag_x = imuInCar.C(:,8);
c_mag_y = imuInCar.C(:,9);
c_mag_z = imuInCar.C(:,10);
c_acc_x = imuInCar.C(:,48);
c_acc_y= imuInCar.C(:,49);
c_acc_z = imuInCar.C(:,50);
c_gyro_x = imuInCar.C(:,36);
c_gyro_y = imuInCar.C(:,37);
c_gyro_z = imuInCar.C(:,38);

acc_x1 = table2array(c_acc_x);
timeic1 = table2array(timeic);
c_vel_x = cumtrapz(acc_x1,timeic1);

%{
for i=1:173
    e = utm_east{:,i};
    n = utm_north{:,i};
    plot(e, n)
end
%}

%{
for i=1:1010
    d = timeg{1,i};
    %size(d)
    vel_xx = vel_x(,i);
    plot(d, vel_xx, '.');
end
%}
%{
for i=1:6932      
    t = c_acc_x{:,i};
    r = timeic{:,i};
    figure()
    plot(r, t)
    title('Acceleration along x vs time')
end

%}
for i=1:6932
    r = timeic{:,i};
    vx = c_vel_x;
    figure()
    plot(r, vx)
    title('Velocity along x vs time')
end

for i=1:6932
    q = c_acc_y{:,i};
    b = timeic{:,i};
    figure 
    plot(b, q)
    title('Acceleration along y vs time')
    end

for i=1:6932
    u = c_acc_z{:,i};
    s = timeic{:,i};
    figure 
    plot(s, u)
    title('Acceleration along z vs time')
end