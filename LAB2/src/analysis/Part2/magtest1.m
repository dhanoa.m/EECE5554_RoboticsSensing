clear all

imuinCircles = load ('imuinCircles.mat')

ox = imuinCircles.D(:,23);
oy = imuinCircles.D(:,24);
oz = imuinCircles.D(:,25);
ow = imuinCircles.D(:,26);

c_mag_x = imuinCircles.D(:,8);
c_mag_y = imuinCircles.D(:,9);
c_mag_z = imuinCircles.D(:,10);
c_acc_x = imuinCircles.D(:,48);
c_acc_y= imuinCircles.D(:,49); 
c_acc_z = imuinCircles.D(:,50);
c_gyro_x = imuinCircles.D(:,36);
c_gyro_y = imuinCircles.D(:,37);
c_gyro_z = imuinCircles.D(:,38);

%Magnetometer data Plot
figure 
subplot(2,2,1)
for i=1:6932
    t = c_mag_x{:,i};
    r = c_mag_y{:,i};
    plot(t, r)
    title('Original Mag_X and Mag_Y Data')
    xlabel('X-axis'); 
    ylabel('Y-axis');
end

subplot(2,2,2)

plot(imu_time,mag_measurements(:,1));
title('Plot of magnetometer X readings wrt time');
xlabel('time'); 
ylabel('Magnetometer-X');


