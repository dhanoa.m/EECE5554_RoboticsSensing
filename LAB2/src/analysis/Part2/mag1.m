clear all

imuinCircles = load ('imuinCircles.mat')

ox = imuinCircles.D(:,23);
oy = imuinCircles.D(:,24);
oz = imuinCircles.D(:,25);
ow = imuinCircles.D(:,26);

c_mag_x = imuinCircles.D(:,8);
c_mag_y = imuinCircles.D(:,9);
c_mag_z = imuinCircles.D(:,10);
c_acc_x = imuinCircles.D(:,48);
c_acc_y= imuinCircles.D(:,49);
c_acc_z = imuinCircles.D(:,50);
c_gyro_x = imuinCircles.D(:,36);
c_gyro_y = imuinCircles.D(:,37);
c_gyro_z = imuinCircles.D(:,38);

function [S_correct, H_correct] = get_mag_correction(c_mag_x, c_mag_y, plotting)

figure
subplot(2,2,1)
plot(c_mag_x, c_mag_y, '.')
title('Original Mag_X and Mag_Y Data')
axis('equal')
ax = gca;
ellipse_best_fit = fit_ellipse(c_mag_x, c_mag_y, ax);
%{
else
    ellipse_best_fit = fit_ellipse(c_mag_x, c_mag_y);
end
%}
H_correct = [-ellipse_best_fit.X0_in; -ellipse_best_fit.Y0_in];

shifted_points = [c_mag_x; c_mag_y] + H_correct;


figure
subplot(2,2,2)
plot(shifted_points(1,:),shifted_points(2,:), '.')
title('Shifted to Origin Mag_X and Mag_Y Data')
axis('equal')
ax = gca;
fit_ellipse(shifted_points(1,:), shifted_points(2,:), ax);
end
%{
sin_phi = sin(-ellipse_best_fit.phi);
cos_phi = cos(-ellipse_best_fit.phi);
R = [ cos_phi sin_phi; -sin_phi cos_phi ];
rotated_points = R*[shifted_points(1,:); shifted_points(2,:)];

%if plotting
    figure
    plot(rotated_points(1,:), rotated_points(2,:), '.')
    title('Rotated Mag_X and Mag_Y Data')
    axis('equal')
    ax = gca;
    fit_ellipse(rotated_points(1,:), rotated_points(2,:), ax);
%end

max_x = max(rotated_points(1,:));
max_y = max(rotated_points(2,:));

if (max_x > max_y)
    scale_param = ellipse_best_fit.short_axis/ellipse_best_fit.long_axis;
else
    scale_param = ellipse_best_fit.long_axis/ellipse_best_fit.short_axis;
end

rotated_points(1,:)= rotated_points(1,:).*scale_param;

sin_phi = sin(ellipse_best_fit.phi);
cos_phi = cos(ellipse_best_fit.phi);
R = [ cos_phi sin_phi; -sin_phi cos_phi ];

final_points = R*[rotated_points(1,:); rotated_points(2,:)];

s11 = scale_param*(cos(ellipse_best_fit.phi)^2) + sin(ellipse_best_fit.phi)^2;
s12 = -scale_param*cos(ellipse_best_fit.phi)*sin(ellipse_best_fit.phi) + sin(ellipse_best_fit.phi)*cos(ellipse_best_fit.phi);
s21 = -scale_param*sin(ellipse_best_fit.phi)*cos(ellipse_best_fit.phi) + cos(ellipse_best_fit.phi)*sin(ellipse_best_fit.phi);
s22 = scale_param*(sin(ellipse_best_fit.phi)^2) + cos(ellipse_best_fit.phi)^2;

S_correct = [s11, s12; s21, s22];

if plotting
    figure
    plot(rotated_points(1,:), rotated_points(2,:), '.')
    title('Scaled Mag_X and Mag_Y Data')
    axis('equal')
    ax = gca;
    fit_ellipse(rotated_points(1,:), rotated_points(2,:), ax);
    
    figure
    plot(final_points(1,:), final_points(2,:), '.')
    title('Scaled and Rotated Back to Original Position Mag_X and Mag_Y Data')
    axis('equal')
    ax = gca;
    fit_ellipse(final_points(1,:), final_points(2,:), ax);
end

end

 %plot(c_mag_x, c_mag_y, '.')
 %plot(shifted_points(1,:),shifted_points(2,:), '.')
 %plot(rotated_points(1,:), rotated_points(2,:), '.')
 %plot(rotated_points(1,:), rotated_points(2,:), '.')
 %plot(final_points(1,:), final_points(2,:), '.')
%}