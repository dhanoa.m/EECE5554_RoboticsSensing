clear all

imuInCar = load ('imuInCar.mat')
imu_data = imuInCar;
imu_time = imu_data.C(:,1);
imu_time = table2array(imu_time);
%imu_time_real = imu_time(3065:6331,:);
imu_time_diff = diff(imu_time);
imu_orientation = imu_data.C(:,23:26);
imu_ang_vel = imu_data.C(:,36:38);
imu_lin_acc = imu_data.C(:,48:50);
imu_lin_acc = table2array(imu_lin_acc);
mag_measurements = imu_data.C(:,8:10);
mag_measurements = table2array(mag_measurements);

gpsInCar = load('gpsInCar.mat')
gps_data = gpsInCar;
gps_time = gps_data.A(:,1);
gps_time = table2array(gps_time);
gps_time_diff = diff(gps_time);
gps_measurements = gps_data.A(:,8:9);
gps_measurements = table2array(gps_measurements);

%{
figure 
subplot(2,2,1)
plot(imu_lin_acc(:,1),imu_lin_acc(:,2));
title('Plot of magnetometer readings(X and Y)');
xlabel('X-axis'); 
ylabel('Y-axis');

subplot(2,2,2)
%}
figure
plot(imu_time,imu_lin_acc(:,1));
title('Plot of Acceleration X readings wrt time');
xlabel('time'); 
ylabel('Acceleration-X');
grid on
hold on
j = imu_lin_acc(:,1);
 
 for i=7207:9647
     j = j + 0.796;
 end
 for i=9648:12269
     j = j + 0.978;
 end
 for i=12270:14181
     j = j + 0.823;
 end  
j = j - 6080.1;
figure 

plot(imu_time,j);
title('Plot of Acceleration X corrected readings wrt time');
xlabel('time'); 
ylabel('Acceleration-X');
grid on
hold on

velocity = cumtrapz(imu_time, j);
velocity = velocity/10^9;
plot(imu_time,velocity);
grid on
%gps_east_diff = []
%gps_north_diff = []
for i = 1:1009
    gps_east_diff(i) = gps_measurements(i+1,1)-gps_measurements(i,1);
    gps_north_diff(i) = gps_measurements(i+1,2)-gps_measurements(i,2);
    %gps_east_diff = table2array(gps_east_diff);
    %gps_north_diff = table2array(gps_north_diff);
end
figure
%gps_east_diff = gps_east_diff/gps_time;
%gps_north_diff = gps_north_diff/gps_time;
velocity1 = sqrt(gps_north_diff.^2+gps_east_diff.^2);

gps_tim = gps_time(1:1009,1);
%size(gps_tim)
size(velocity1)
plot(gps_tim, velocity1);


%Dead Reckoning with IMU

ang_vel_z = imu_ang_vel(:,3);
ang_vel_z = table2array(ang_vel_z);
part1 = ang_vel_z .* velocity;
figure
plot(imu_time, part1, 'r');
hold on
lin_acc_y = imu_lin_acc(:,2);
lin_acc_y = lin_acc_y + 0.3;
plot(imu_time, lin_acc_y, 'b');

% part2

