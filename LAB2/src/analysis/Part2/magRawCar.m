clear all

imuinCar = load ('imuInCar.mat')
imu_data = imuinCar;

imu_time = imu_data.C(:,1);
imu_time = table2array(imu_time);
imu_orientation = imu_data.C(:,23:26);
imu_ang_vel = imu_data.C(:,36:38);
imu_lin_acc = imu_data.C(:,48:50);

mag_measurements = imu_data.C(:,8:10);
mag_measurements = table2array(mag_measurements);

sz = 40;

%Magnetometer data Plot
figure 
subplot(2,2,1)
plot(mag_measurements(:,1),mag_measurements(:,2));
title('Plot of magnetometer readings(X and Y)');
xlabel('X-axis'); 
ylabel('Y-axis');

subplot(2,2,2)
plot(imu_time,mag_measurements(:,1));
title('Plot of magnetometer X readings wrt time');
xlabel('time'); 
ylabel('Magnetometer-X');

subplot(2,2,3)
plot(imu_time,mag_measurements(:,2));
title('magnetometer data: y v/s t');
title('Plot of magnetometer Y readings wrt time'); 
xlabel('time'); 
ylabel('Magnetometer-Y');

subplot(2,2,4)
plot(imu_time,mag_measurements(:,3));
title('magnetometer data: z v/s t');
title('Plot of magnetometer Z readings wrt time'); 
xlabel('time'); 
ylabel('Magnetometer-Z');
