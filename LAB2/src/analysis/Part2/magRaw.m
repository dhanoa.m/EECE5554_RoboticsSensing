clear all

imuinCircles = load ('imuinCircles.mat')
imu_data = imuinCircles;

imu_time = imu_data.D(:,1);
imu_time = table2array(imu_time);
imu_orientation = imu_data.D(:,23:26);
imu_ang_vel = imu_data.D(:,36:38);
imu_lin_acc = imu_data.D(:,48:50);

mag_measurements = imu_data.D(:,8:10);
mag_x = imu_data.D(:,8);
mag_x = table2array(mag_x);
mag_y = imu_data.D(:,9);
mag_y = table2array(mag_y);
mag_z = imu_data.D(:,10);
mag_z = table2array(mag_z);
mag_measurements = table2array(mag_measurements);

sz = 40;

%Magnetometer data Plot
figure 
%subplot(2,2,1)
plot(mag_measurements(:,1),mag_measurements(:,2));
title('Plot of magnetometer readings(X and Y)');
xlabel('X-axis'); 
ylabel('Y-axis');
%{
subplot(2,2,2)
plot(imu_time,mag_measurements(:,1));
title('Plot of magnetometer X readings wrt time');
xlabel('time'); 
ylabel('Magnetometer-X');

subplot(2,2,3)
plot(imu_time,mag_measurements(:,2));
title('magnetometer data: y v/s t');
title('Plot of magnetometer Y readings wrt time'); 
xlabel('time'); 
ylabel('Magnetometer-Y');

subplot(2,2,4)
plot(imu_time,mag_measurements(:,3));
title('magnetometer data: z v/s t');
title('Plot of magnetometer Z readings wrt time'); 
xlabel('time'); 
ylabel('Magnetometer-Z');
%}
%Hard iron
alpha = (max(mag_x(3065:6331)) + min(mag_x(3065:6331)))/2;
beta = (max(mag_y(3065:6331)) + min(mag_y(3065:6331)))/2;
center_x = mean(mag_x(3065:6331));
center_y = mean(mag_y(3065:6331));



for i = 3065:6331
    hard_correct_mag_x(i) = mag_x(i) - alpha;
    hard_correct_mag_y(i) = mag_y(i) - beta;
    r(i) = sqrt(hard_correct_mag_x(i)^2 + hard_correct_mag_y(i)^2);
end



center_x = mean(mag_x(3065:6331));
center_y = mean(mag_y(3065:6331));
szigma = 1.1;



figure
plot(hard_correct_mag_x, hard_correct_mag_y)
hold on
xL = xlim;
yL = ylim;
line([0 0], yL);  
line(xL, [0 0]);


for i = 3065:6331
    shard_correct_mag_x(i) = hard_correct_mag_x(i)/szigma;
    shard_correct_mag_y(i) = hard_correct_mag_y(i);
end

figure
plot(shard_correct_mag_x, shard_correct_mag_y)
hold on
xL = xlim;
yL = ylim;
line([0 0], yL);  
line(xL, [0 0]);

for i = 3065:6331
    if mag_y(i) > 0 && mag_x(i) > 0
        inc_angle(i) = - atan(mag_y(i)/mag_x(i))*(180/pi);
    end
    if mag_y(i) > 0 && mag_x(i) < 0
        inc_angle(i) = -180 - atan(mag_y(i)/mag_x(i))*(180/pi);
    end
    if mag_y(i) < 0 && mag_x(i) < 0
        inc_angle(i) = 180 - atan(mag_y(i)/mag_x(i))*(180/pi);
    end
    if mag_y(i) < 0 && mag_x(i) > 0
        inc_angle(i) = - atan(mag_y(i)/mag_x(i))*(180/pi);
    end
    
       
    if hard_correct_mag_y(i) > 0 && hard_correct_mag_x(i) > 0
        shc_angle(i) = - atan(hard_correct_mag_y(i)/hard_correct_mag_x(i))*(180/pi);
    end
    if hard_correct_mag_y(i) > 0 && hard_correct_mag_x(i) < 0
        shc_angle(i) = -180 - atan(hard_correct_mag_y(i)/hard_correct_mag_x(i))*(180/pi);
    end
    if hard_correct_mag_y(i) < 0 && hard_correct_mag_x(i) < 0
        shc_angle(i) = 180 - atan(hard_correct_mag_y(i)/hard_correct_mag_x(i))*(180/pi);
    end
    if hard_correct_mag_y(i) < 0 && hard_correct_mag_x(i) > 0
        shc_angle(i) = - atan(hard_correct_mag_y(i)/hard_correct_mag_x(i))*(180/pi);
    end
end



