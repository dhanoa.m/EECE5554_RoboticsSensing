clear all

imuInCar = load ('imuInCar.mat')
imu_data = imuInCar;
gpsInCar = load('gpsInCar.mat')
gps_data = gpsInCar;
gps_time = gps_data.A(:,1);
gps_measurements = gps_data.A(:,8:9);
gps_measurements = table2array(gps_measurements);

imu_time = imu_data.C(:,1);
imu_time = table2array(imu_time);
imu_orientation = imu_data.C(:,23:26);
imu_ang_vel = imu_data.C(:,36:38);
imu_lin_acc = imu_data.C(:,48:50);
imu_lin_acc = table2array(imu_lin_acc);

mag_measurements = imu_data.C(:,8:10);
mag_x = imu_data.C(:,8);
mag_x = table2array(mag_x);
mag_y = imu_data.C(:,9);
mag_y = table2array(mag_y);
mag_z = imu_data.C(:,10);
mag_z = table2array(mag_z);
mag_measurements = table2array(mag_measurements);

sz = 40;
%{
%Magnetometer data Plot
figure 
%subplot(2,2,1)
plot(mag_measurements(:,1),mag_measurements(:,2));
title('Plot of magnetometer readings(X and Y)');
xlabel('X-axis'); 
ylabel('Y-axis');
%}
for i = 2:39745
    bias = [mean(imu_lin_acc(i,1)) mean(imu_lin_acc(i,2)) mean(imu_lin_acc(i,3))];
    imu_lin_acc_unbiased = [imu_lin_acc(:,1)-bias(1) imu_lin_acc(:,2)-bias(2) imu_lin_acc(:,3)-bias(3)];
end

imu_time_real = imu_time(3065:6331,:);
imu_orientation_real = imu_orientation(3065:6331,:);
imu_orientation_real = table2array(imu_orientation_real);
imu_ang_vel_real = imu_ang_vel(3065:6331,:);
imu_ang_vel_real = table2array(imu_ang_vel_real);
imu_lin_acc_real = imu_lin_acc_unbiased(3065:6331,:);

mag_time_real = imu_time(3065:6331,:);
mag_measurements_real = mag_measurements(3065:6331,:);


%Hard iron
temp_mag_circle = [mag_measurements(3065:6331,1:2)];

figure 
axis equal;
plot(mag_measurements_real(:,1),mag_measurements_real(:,2));
title('Plot of magnetometer readings (Y vs X)'); xlabel('X-readings'); ylabel('Y-readings');

figure
axis equal;
plot(temp_mag_circle(:,1),temp_mag_circle(:,2));
title('Plot of magnetometer readings for circles around Ruggles circle(Y vs X)'); xlabel('X-readings'); ylabel('Y-readings');

alpha = (max(temp_mag_circle(:,1))+min(temp_mag_circle(:,1)))/2;
beta = (max(temp_mag_circle(:,2))+min(temp_mag_circle(:,2)))/2;
mag_corrected_values_real = [mag_measurements_real(:,1)-alpha, mag_measurements_real(:,2)-beta];
temp_mag_circle_corrected = [temp_mag_circle(:,1)-alpha, temp_mag_circle(:,2)-beta];


figure
axis equal;
plot(temp_mag_circle_corrected(:,1),temp_mag_circle_corrected(:,2));
title('Plot of magnetometer readings for circles after adjustments for Hard Iron effects'); xlabel('X-readings'); ylabel('Y-readings');



%Soft iron effect
r = ((temp_mag_circle_corrected(:,1).^2) + (temp_mag_circle_corrected(:,2).^2)).^0.5;
[m_v,m_I] = min(r);
[r_v,r_I] = max(r);

theta = asind(temp_mag_circle_corrected(r_I,2)/r_v)
R = [cosd(theta) sind(theta); -sind(theta) cosd(theta)];
mag_corrected_values_real = R*mag_corrected_values_real';
v_1 = R*temp_mag_circle_corrected';

scale_factor = m_v/r_v;
mag_corrected_values_real(1,:) = mag_corrected_values_real(1,:).*scale_factor;
v_1(1,:) = v_1(1,:).*scale_factor;
theta = -theta;
R = [cosd(theta) sind(theta); -sind(theta) cosd(theta)];
mag_corrected_values_real = R*mag_corrected_values_real;
v_1 = R*v_1;

figure 
axis equal;
plot(v_1(1,:),v_1(2,:));
title('Plot of magnetometer readings for circles after adjustments for Soft Iron effects'); xlabel('X-readings'); ylabel('Y-readings');

figure
axis equal; axis square;
plot(mag_corrected_values_real(1,:),mag_corrected_values_real(2,:));
title('Plot of magnetometer readings after adjustments for Soft and Hard Iron effects'); xlabel('X-readings'); ylabel('Y-readings');

mag_corrected_values_real = mag_corrected_values_real';

%Calculating yaw angles:
yaw_mag = atand(mag_corrected_values_real(:,1)./mag_corrected_values_real(:,2));

imu_time_diff = (imu_time_real-imu_time_real(1))./10^9;
%imu_time_diff = table2array(imu_time_diff );
yaw_ang_vel_rad = cumtrapz(imu_time_diff, imu_ang_vel_real(:,3));
yaw_ang_vel_deg = wrapTo180(rad2deg(yaw_ang_vel_rad));

eul = quat2eul([imu_orientation_real(:,4), imu_orientation_real(:,1), imu_orientation_real(:,2), imu_orientation_real(:,3)]);
yaw_eul = wrapTo180(rad2deg(eul(:,1)));

figure
plot(imu_time_real, unwrap(yaw_mag), 'r');
hold on
plot(imu_time_real, unwrap(yaw_ang_vel_deg), 'b');
plot(imu_time_real, unwrap(yaw_eul), 'g');
title('Plot of yaw calculated from magnetometer, gyro and orientation'); 
%legend('Magnetometer', 'Gyro', 'Orientation');
xlabel('time'); 
ylabel('yaw angle (degrees)');
hold off


a = 0.4;
comp_fil_yaw = (yaw_mag.*a)+(yaw_ang_vel_deg.*(1-a));

figure
plot(imu_time_real,unwrap(comp_fil_yaw));
hold on
plot(imu_time_real,unwrap(yaw_eul));
title('Plot of yaw calculated using Complimentary filter and from orientation'); 
legend({'Complimentary Filter','Orientation'}); 
xlabel('time'); 
ylabel('yaw angle (degrees)');
hold off

%Dead Reckoning with IMU Part2

j = imu_lin_acc(:,1);
 
 for i=7207:9647
     j = j + 0.796;
 end
 for i=9648:12269
     j = j + 0.978;
 end
 for i=12270:14181
     j = j + 0.823;
 end  
j = j - 6080.1;
figure 

plot(imu_time,j);
title('Plot of Acceleration X corrected readings wrt time');
xlabel('time'); 
ylabel('Acceleration-X');
grid on
hold on

velocity = cumtrapz(imu_time, j);
velocity = velocity/10^9;
velocity = velocity(1:3267,1);
%{
sinQ = sin(yaw_ang_vel_deg);
U_East = gps_measurements(:,1);
sinQ = sinQ(1:1010,1);
one= U_East .* sinQ;
size(one)
one1 = cumtrapz(gps_time, one(:,1));
%}
size(velocity)
size(comp_fil_yaw)
v_east = velocity.*cos(deg2rad(comp_fil_yaw));
v_north = velocity.*sin(deg2rad(comp_fil_yaw));

x_n = cumtrapz(mag_time_real./10^9,v_north);
x_e = cumtrapz(mag_time_real./10^9,v_east);

figure
plot(gps_time,gps_measurements(:,1)-gps_measurements(1,1));
hold on
plot(imu_time_real,x_e);
title('Plot of Displacements obtained from GPS and IMU (X-axis)'); legend('GPS','IMU');xlabel('time'); ylabel('X-axis/UTM Easting');
hold off

figure
plot(gps_time,gps_measurements(:,2)-gps_measurements(1,2));
hold on
plot(imu_time_real,x_n);
title('Plot of Displacements obtained from GPS and IMU (Y-axis)'); legend('GPS','IMU');xlabel('time'); ylabel('Y-axis/UTM Northing');
hold off

th = -50;
R = [cosd(th) sind(th); -sind(th) cosd(th)];
scale = 0.5;

x_e = -1.*x_e;
% x_n = -1.*x_n;

imu_trajectory = scale.*(R*[x_e x_n]');

figure
plot(imu_trajectory(1,:),imu_trajectory(2,:));
hold on
plot(gps_measurements(:,1)-gps_measurements(1,1),gps_measurements(:,2)-gps_measurements(1,2));
title('Plot of trajectories obtained from GPS and IMU'); legend('IMU','GPS');xlabel('X-axis/UTM Easting'); ylabel('Y-axis/UTM Northing');
hold off




