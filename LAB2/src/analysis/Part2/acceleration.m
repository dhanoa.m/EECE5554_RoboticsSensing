clear all

imuInCar = load ('imuInCar.mat')
imu_data = imuInCar;
imu_time = imu_data.C(:,1);
imu_time = table2array(imu_time);
imu_orientation = imu_data.C(:,23:26);
imu_ang_vel = imu_data.C(:,36:38);
imu_ang_vel = table2array(imu_ang_vel);
imu_lin_acc = imu_data.C(:,48:50);
imu_lin_acc = table2array(imu_lin_acc);
mag_measurements = imu_data.C(:,8:10);
mag_measurements = table2array(mag_measurements);

gpsInCar = load('gpsInCar.mat')
gps_data = gpsInCar;
gps_time = gps_data.A(:,1);
gps_time = table2array(gps_time);
gps_measurements = gps_data.A(:,8:9);
gps_measurements = table2array(gps_measurements);
%{
figure 
subplot(2,2,1)
plot(imu_time,imu_lin_acc(:,1));
title('Plot of Acceleration X readings wrt time');
xlabel('time'); 
ylabel('Acceleration-X');
hold on
%}
vel_lin = cumtrapz(imu_time, imu_lin_acc(:,1));

plot(imu_time,vel_lin);
title('Plot of Velocity wrt Time');
hold on 


imu_velocity = cumtrapz(imu_time_diff, imu_lin_acc_real(:,1));

gps_velocity = zeros(size(gps_measurements_real,1));
for c = 1:size(gps_measurements_real,1)-1
     temp = gps_measurements_real(c+1,:)-gps_measurements_real(c,:);
     gps_velocity(c) = ((temp(1)^2 +temp(2)^2)^0.5)/((gps_time(c+1)-gps_time(c))/10^9);
end

figure
plot(gps_time_real,gps_velocity);
hold on
plot(imu_time_real(1:size(imu_velocity)), imu_velocity);
title('Plot of velocities from GPS and from IMU'); legend('GPS Velocity','IMU Velocity');xlabel('time'); ylabel('velocity');
hold off

%figure

%plot(imu_time,imu_ang_vel);




