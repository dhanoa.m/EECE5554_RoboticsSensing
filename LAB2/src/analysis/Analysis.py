#!/usr/bin/env python 

from tf.transformations import euler_from_matrix, euler_from_quaternion
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import rosbag
import csv
import math

anayaw=[]
yaw=[]
anapitch=[]
pitch=[]
anaroll=[]
roll=[]

anamagx=[]
anamagy=[]
anamagz=[]

anaaccx=[]
anaaccy=[]
anaaccz=[]

angvelx=[]
angvely=[]
angvelz=[]

anagyrox=[]
anagyroy=[]
anagyroz=[]

time=[]

bag =rosbag.Bag('/home/minner/catkin_ws/src/imu/final_imu.bag')
for topic,Min,t in bag.read_messages(topics=['/Min']):
    anaaccx.append(Min.linear_acceleration.x)
    anaaccy.append(Min.linear_acceleration.y)
    anaaccz.append(Min.linear_acceleration.z)

    angvelx.append(Min.angular_velocity.x)
    angvely.append(Min.angular_velocity.y)
    angvelz.append(Min.angular_velocity.z)


    rpy = [Min.orientation.x, Min.orientation.y, Min.orientation.z, Min.orientation.w]
    z = euler_from_quaternion(rpy)
    yaw.append(z[0])
    pitch.append(z[1])
    roll.append(z[2])
    time.append(Min.header.stamp.secs)

for topic,Ner,t in bag.read_messages(topics=['/Ner']):
    anamagx.append(Ner.magnetic_field.x)
    anamagy.append(Ner.magnetic_field.y)
    anamagz.append(Ner.magnetic_field.z)
    
accx = np.array(anaaccx)
meaccx=np.mean(accx)
Time = np.array(time)
#print(meaccx)
accy = np.array(anaaccy)
meaccy=np.mean(accy)
#print(meaccy)
accz = np.array(anaaccz)
meaccz=np.mean(accz)
#print(meaccz)

gvelx=np.array(angvelx)
gvely=np.array(angvely)
gvelz=np.array(angvelz)

magx = np.array(anamagx)
magy = np.array(anamagy)
magz = np.array(anamagz)

yaw_fin = np.array(yaw)
pitch_fin = np.array(pitch)
roll_fin = np.array(roll)
#print(np.degrees(yaw_fin))
y1= np.degrees(yaw_fin)
p1=np.degrees(pitch_fin)
r1=np.degrees(roll_fin)

'''
print(magx.size)
print(Time.size)
print(magy.size)
print(magz.size)
'''

plt.figure(0)
plt.subplot(231)
plt.plot(Time,accx,'.')
#print((accx-np.mean(accx)))
std_accx=np.std(accx)
print(std_accx)
plt.xlabel('Time')
plt.ylabel('Acceleration x')
plt.title('Time vs Acceleration on x')
#std_accx= np.std(accx)
#print(std_accx)

plt.subplot(232)
plt.plot(Time,accy,'.')
#print(np.mean(accy-np.mean(accy)))
std_accy=np.std(accy)
print(std_accy)
plt.xlabel('Time')
plt.ylabel('Acceleration y')
plt.title('Time vs Acceleration on y')

plt.subplot(233)
plt.plot(Time,accz,'.')
#print(np.mean(accz-np.mean(accz)))
std_accz=np.std(accz)
print(std_accz)
plt.xlabel('Time')
plt.ylabel('Acceleration z')
plt.title('Time vs Acceleration on z')

plt.subplot(234)
plt.hist(accx-np.mean(accx))
#print('axe=',abs(min(accx-np.mean(accx)))-abs(max(accx-np.mean(accx))))
plt.xlabel('Error along Acceleration x')
plt.ylabel('No. of Readings')
plt.title('Error Distribution Acceleration x')
    
plt.subplot(235)
plt.hist(accy-np.mean(accy))
plt.xlabel('Error along Acceleration y')
plt.ylabel('No. of Readings')
plt.title('Error Distribution Acceleration y')
#plt.subplot(235)
#plt.plot(Time,magy,'.')
plt.subplot(236)
plt.hist(accz-np.mean(accz))
plt.xlabel('Error along Acceleration z')
plt.ylabel('No. of Readings')
plt.title('Error Distribution Acceleration z')



plt.figure(1)
plt.subplot(231)
plt.plot(Time,magx,'.')

plt.subplot(232)
plt.plot(Time,magy,'.')

plt.subplot(233)
plt.plot(Time,magz,'.')

plt.subplot(234)
plt.hist(magx)

plt.subplot(235)
plt.hist(magy)

plt.subplot(236)
plt.hist(magz)



plt.figure(2)
plt.subplot(231)
plt.plot(Time,gvelx,'.')
#print((accx-np.mean(accx)))
std_gvelx=np.std(gvelx)
print(std_gvelx)
plt.xlabel('Time')
plt.ylabel('Angular Velocity x')
plt.title('Time vs Angular Velocity on x')
#std_accx= np.std(accx)
#print(std_accx)

plt.subplot(232)
plt.plot(Time,gvely,'.')
#print(np.mean(accy-np.mean(accy)))
std_gvely=np.std(gvely)
print(std_gvely)
plt.xlabel('Time')
plt.ylabel('Angular Velocity y')
plt.title('Time vs Angular Velocity on y')

plt.subplot(233)
plt.plot(Time,gvelz,'.')
#print(np.mean(accz-np.mean(accz)))
std_gvelz=np.std(gvelz)
print(std_gvelz)
plt.xlabel('Time')
plt.ylabel('Angular VElocity z')
plt.title('Time vs Angular Velocity on z')

plt.subplot(234)
plt.hist(gvelx-np.mean(gvelx))
plt.xlabel('Error along Angular Velocity x')
plt.ylabel('No. of Readings')
plt.title('Error Distribution Agular Velocity x')

plt.subplot(235)
plt.hist(gvely-np.mean(gvely))
plt.xlabel('Error along Angular Velocity y')
plt.ylabel('No. of Readings')
plt.title('Error Distribution Angular Velocity y')
#plt.subplot(235)
#plt.plot(Time,magy,'.')
plt.subplot(236)
plt.hist(gvelz-np.mean(gvelz))
plt.xlabel('Error along Angular Velocity z')
plt.ylabel('No. of Readings')
plt.title('Error Distribution Angular Velocity z')



plt.figure(3)
plt.subplot(231)
plt.plot(Time,y1,'.')
#print((accx-np.mean(accx)))
std_y1=np.std(y1)
print(std_y1)
plt.xlabel('Time')
plt.ylabel('Yaw')
plt.title('Time vs Yaw')
#std_accx= np.std(accx)
#print(std_accx)

plt.subplot(232)
plt.plot(Time,p1,'.')
#print(np.mean(accy-np.mean(accy)))
std_p1=np.std(p1)
print(std_p1)
plt.xlabel('Time')
plt.ylabel('Pitch')
plt.title('Time vs Pitch')

plt.subplot(233)
plt.plot(Time,r1,'.')
#print(np.mean(accz-np.mean(accz)))
std_r1=np.std(r1)
print(std_r1)
plt.xlabel('Time')
plt.ylabel('Roll')
plt.title('Time vs Roll')

plt.subplot(234)
plt.hist(y1-np.mean(y1))
plt.xlabel('Error Yaw')
plt.ylabel('No. of Readings')
plt.title('Error Distribution Yaw')

plt.subplot(235)
plt.hist(p1-np.mean(p1))
plt.xlabel('Error Pitch')
plt.ylabel('No. of Readings')
plt.title('Error Distribution Pitch')
#plt.subplot(235)
#plt.plot(Time,magy,'.')
plt.subplot(236)
plt.hist(r1-np.mean(r1))
plt.xlabel('Error Roll')
plt.ylabel('No. of Readings')
plt.title('Error Distribution Roll')


plt.show()