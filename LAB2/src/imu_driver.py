#!/usr/bin/env python 

import rospy
import serial
#import utm
import math
from sensor_msgs.msg import Imu, MagneticField

#from std_msgs.msg import Float64 
from std_msgs.msg import Header
#from std_msgs.msg import String
from tf.transformations import quaternion_from_euler
#from gps_driver.msg import custom



def driver_fun(extract):
    data = extract.readline()
    return data

if __name__ == '__main__':
    port = serial.Serial('/dev/ttyUSB0' ,115200, timeout=3)
    rospy.init_node('Imu')
    sensor_pub = rospy.Publisher('Min', Imu, queue_size=5)
    sensor2_pub = rospy.Publisher('Ner', MagneticField, queue_size=5)

    try:
        while not rospy.is_shutdown():
            line = driver_fun(port)
            data = line.split(',')
            beta = Imu()
            papa = MagneticField()
            if data[0] == '':
                rospy.logwarn ('Empty')

            elif data[0].strip() == '$VNYMR':
                beta.header=Header()
                beta.header.stamp=rospy.Time.now()

                yaw = float(data[1])
                radyaw = math.radians(yaw)
                pitch = float(data[2])
                radpitch = math.radians(pitch)
                roll = float(data[3])
                radroll = math.radians(roll)

                quat = quaternion_from_euler(radyaw,radpitch,radroll)

                beta.orientation.x = quat[0]
                beta.orientation.y = quat[1]
                beta.orientation.z = quat[2]
                beta.orientation.w = quat[3]

                beta.angular_velocity.x = float(data[10])
                beta.angular_velocity.y = float(data[11])
                zed = data[12].split('*')
                beta.angular_velocity.z = float(zed[0])

                beta.linear_acceleration.x = float(data[7])
                beta.linear_acceleration.y = float(data[8])
                beta.linear_acceleration.z = float(data[9])

                papa.header=Header()
                papa.header.stamp = rospy.Time.now()

                papa.magnetic_field.x = float(data[4])
                papa.magnetic_field.y = float(data[5])
                papa.magnetic_field.z = float(data[6])

                sensor_pub.publish(beta)
                sensor2_pub.publish(papa)

            else:
                print('No gps')

    except rospy.ROSInterruptException:
        pass

    except serial.serialutil.SerialException:
        rospy.loginfo("Sensor shut down)")

