#!/usr/bin/env python 

# -*- coding: utf-8 -*- 

 

import rospy 
import serial 
#from math import sin, pi 
import utm
from std_msgs.msg import Float64 
from std_msgs.msg import Header
from std_msgs.msg import String
from gps_driver.msg import custom
#import rosbag

'''
import bagpy
from bagpy import bagreader
import pandas as pd
import numpy as np
b = bagreader('SatationaryDataFile.bag')
dfs = pd.read_csv(b.)
'''
#from nav_msgs.msg import Odomet 3g		ry 

 

def driver_fun(extract):
    data = extract.readline()
    return data


port = serial.Serial('/dev/ttyUSB0' ,4800, timeout=3)
#pub = rospy.Publisher('gps/header' ,Header, queue_size=10)
rospy.init_node('GPS')
gps_pub = rospy.Publisher('spg', custom, queue_size=5)
'''
lat_pub = rospy.Publisher('gps/lat', String, queue_size=5)
lon_pub = rospy.Publisher('gps/lon', String, queue_size=5)
altitude_pub = rospy.Publisher('gps/alt', Float64, queue_size=5)
utm_easting_pub = rospy.Publisher('gps/utm_easting', Float64, queue_size=5)
utm_northing_pub = rospy.Publisher('gps/utm_northing', Float64, queue_size=5)
utm_zone = rospy.Publisher('gps/utm_zone', String, queue_size=5)
#rate = rospy.Rate(10)
#Msg = headers()
#bag = rosbag.Bag('CustomBag.bag' ,'w')
'''
try:
    while not rospy.is_shutdown():
        line = driver_fun(port)
        data = line.split(',')
        beta = custom()
        print(line)
        print(len(data))
        if data[0].strip()=='$GPGGA':
            print('gps')
            beta.header = Header()
            beta.header.stamp = rospy.Time.now()
            beta.Rawdata = line
            #time = data[1]
            #hour = data[0:1]
            #minutes =  data[2:3]
            #seconds = data[4:5]
            #total = int(hour)*3600+(minutes)*60+seconds
            latitude = data[2]
            #latitude = float(latitude)
            lat = float(latitude)/100.0
            longitude = data[4]
            #longitude = float(longitude)
            #lon = ((longitude/100))+(((longitude)%100)/60)
            lon = float(longitude)/100.0
            altitude = float(data[9])
            latidire = data[3]
            longidire = data[5]
            if latidire =='S':
                lat = lat*-1

            if longidire == 'W':
                lon = lon*-1

            latitude = str(lat)+ str(latidire)
            beta.latitude = latitude
            utm_value = utm.from_latlon(lat,lon)
            
            '''
            lat_pub.publish(latitude)
            lon_pub.publish(str(lon)+longidire)
            altitude_pub.publish(altitude)
            '''

            #Longitude = str(lon)+longidire

            utmconvert = utm.from_latlon(lat,lon)
            utm_easting = float(utmconvert[0])
            utm_northing = float(utmconvert[1])
            '''
            utm_easting_pub.publish(utm_easting)
            utm_northing_pub.publish(utm_northing)
            utm_zone.publish(utmconvert[2])
            '''
            beta.longitude = str(lon) + str(longidire)
            beta.Rawdata = line
            beta.Altitude = altitude
            beta.utm_zone = str(utmconvert[2])+str(utmconvert[3])
            beta.utm_easting = utm_easting
            beta.utm_northing = utm_northing
            gps_pub.publish(beta)
            '''beta.header = Header()
            time = extract[1]
            hour = extract[0:1]
            minutes =  extract[2:3]
            seconds = extract[4:5]
            total = int(hour)*3600+(minutes)*60+seconds'''
            '''beta.header.seq = 0
            beta.header.stamp = rospy.Time.now()'''


        else:
            print('No gps')

except rospy.ROSInterruptException:
    pass

except serial.serialutil.SerialException:
    rospy.loginfo("Sensor shut down)")

