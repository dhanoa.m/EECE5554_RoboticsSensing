load static_data1.txt
time = static_data1(:,1);
latitude = static_data1(:,13);
longitude = static_data1(:,14);
altitude = static_data1(:,15);
zone = static_data1(:,16);
utm_easting = static_data1(:,17);
avg_east = mean(utm_easting)
utm_northing = static_data1(:,18);
avg_lat = mean(latitude);
avg_lon = mean(longitude);
avg_alt = mean(altitude);

% For utm_easting
plot(time,utm_easting)
hold on 
plot(time, avg_east,'r.')
deltaeast = abs(utm_easting-avg_east);
erroreast = mean(deltaeast)
percentageDifferenceeast = deltaeast ./ avg_east;
PctDiffeast = mean(percentageDifferenceeast)
%hold on
%plot(time,utm_easting)