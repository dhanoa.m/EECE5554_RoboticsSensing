load static_data1.txt
time = static_data1(:,1);
latitude = static_data1(:,13);
longitude = static_data1(:,14);
altitude = static_data1(:,15);
zone = static_data1(:,16);
utm_easting = static_data1(:,17);
utm_northing = static_data1(:,18);
avg_lat = mean(latitude);
avg_lon = mean(longitude);
avg_alt = mean(altitude);

% For Latitude
plot(time,latitude,'r-',time,avg_lat,'b.')
deltaLat = abs(latitude - avg_lat);
errorLat = mean(deltaLat)
percentageDifferenceLat = deltaLat ./ avg_lat;
%percentageerror = mean(percentageDifference)
PctDiffLat = mean(percentageDifferenceLat)