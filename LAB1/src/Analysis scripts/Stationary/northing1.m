load static_data1.txt
time = static_data1(:,1);
latitude = static_data1(:,13);
longitude = static_data1(:,14);
altitude = static_data1(:,15);
zone = static_data1(:,16);
utm_easting = static_data1(:,17);
utm_northing = static_data1(:,18);
avg_north = mean(utm_northing)
avg_lat = mean(latitude);
avg_lon = mean(longitude);
avg_alt = mean(altitude);

% For utm_northing
%plot(time,utm_easting)
%hold on
plot(time,utm_northing,'b')
hold on 
plot(time, avg_north,'r.')
deltaNorth = abs(utm_northing-avg_north);
errorNorth = mean(deltaNorth)
percentageDifferenceNorth = deltaNorth ./ avg_north;
PctDiffNorth = mean(percentageDifferenceNorth)