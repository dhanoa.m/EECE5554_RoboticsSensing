load moving_data.txt
time = moving_data(:,1);
latitude = moving_data(:,13);
longitude = moving_data(:,14);
altitude = moving_data(:,15);
zone = moving_data(:,16);
utm_easting = moving_data(:,17);

utm_northing = moving_data(:,18);
avg_lat = mean(latitude);
avg_lon = mean(longitude);
avg_alt = mean(altitude);
EastBegin = moving_data(1,17);
EastFinish = moving_data(end,17);
NorthBegin = moving_data(1,18);
NorthFinish = moving_data(end,18);

% For easting v/s northing
plot(utm_northing,utm_easting)
hold on
plot([NorthBegin,NorthFinish],[EastBegin,EastFinish])
