load moving_data.txt
time = moving_data(:,1);
latitude = moving_data(:,13);
longitude = moving_data(:,14);
altitude = moving_data(:,15);
zone = moving_data(:,16);
utm_easting = moving_data(:,17);
utm_northing = moving_data(:,18);


% For Latitude
LatStart = moving_data(1,13);
LatEnd = moving_data(end,13);
timeStart = moving_data(1,1);
timeEnd = moving_data(end,1);
plot([timeStart,timeEnd],[LatStart,LatEnd])
hold on
plot(time,latitude,'r-')
